/*

The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


/*

Labelled For Loop - lfor with the ability to break to a specific loop

Just playing around with a bad programming style but fun to see it working.

*/


#include<iostream>

#define lfor(x, label) \
    try{ \
        for(x)

#define end_lfor(label) \
    }\
    catch(std::string Label){ \
        if(Label.compare(label)!=0){ \
            throw std::string(Label); \
        } \
        std::cout<<"Escaped loop: "<<Label<<std::endl; \
    }
    

#define lbreak(label) \
    throw std::string(label)


int main(){

    lfor(int i=0; i<10; i++, "aLoop"){

        lfor(int j=5; j<9; j++, "bLoop"){
    
            lfor(int k=10; k<14; k++, "cLoop"){

                lfor(int l=15; l<19; l++, "dLoop"){

                    std::cout<<i<<" "<<j<<" "<<k<<" "<<l<<std::endl;
                    
                    if(l==17){
                        lbreak("cLoop");
                    }
         
                }end_lfor("dLoop")

            }end_lfor("cLoop")

        }end_lfor("bLoop")

    }end_lfor("aLoop")

    return 1;

}
